const fs = require('fs').promises;
const path = require('path');

fs.mkdir(path.join(__dirname, 'fileStorage'), 
  { recursive: true }, (err) => { 
    if (err) { 
      return console.error(err); 
    }
});

const createFile = async (filename, content) => {
    await fs.writeFile(`./fileStorage/${filename}`, content, 'utf-8');
}

const getFileList = async () => {
    const files = await fs.readdir('./fileStorage');
    return files;
}

const readFile = async (filename) => {
    const fileContent = await fs.readFile(`./fileStorage/${filename}`, 'utf-8');
    return fileContent;
}

const getFileBirthtime = async (filename) => {
    const { birthtime } = await fs.stat(`./fileStorage/${filename}`);
    return birthtime;
}

module.exports = { createFile, getFileList, readFile, getFileBirthtime };