const express = require('express');
const morgan = require('morgan');
const path = require('path');

const { createFile, getFileList, readFile, getFileBirthtime } = require('./fileFunctions');

const app = express();

app.use(morgan('tiny'));
app.use(express.json());

app.post('/api/files', (req, res) => {
    const allowedExtensions = /(\.log|\.txt|\.json|\.yaml|\.xml|\.js)$/i;
    const reqFilename = req.body.filename;
    const reqContent = req.body.content;

    if (!reqFilename) {
        res.status(400).json({message: 'Please specify \'filename\' parameter'});
        return;
    }

    const isFileValid = allowedExtensions.test(path.extname(reqFilename));
    if (!isFileValid) {
        res.status(400).json({message: 'Invalid file type!'});
        return;
    }

    if (!reqContent) {
        res.status(400).json({message: 'Please specify \'content\' parameter'});
        return;
    }

    createFile(reqFilename, reqContent);
    res.status(200).json({message: 'File created successfully'});
});

app.get('/api/files', async (req, res) => {
    const fileList = await getFileList();
    res.status(200).json({message: 'Success', files: fileList});
});

app.param('filename', async (req, res, next, filename) => {
    const fileList = await getFileList();
    const isFile = fileList.find(item => item === filename)
    if (isFile) {
        next();
    } else {
        res.status(400).json({message: `No file with \'${filename}\' filename found`});
        return;
    }
});

app.get('/api/files/:filename', async (req, res) => {
    const fileContent = await readFile(req.params.filename);
    const ext = path.extname(req.params.filename).slice(1);
    const date = await getFileBirthtime(req.params.filename);
    res.status(200).json({
        message: 'Success',
        filename: `${req.params.filename}`,
        content: fileContent,
        extention: ext,
        uploadedDate: date
    });
});

app.listen(8080);
